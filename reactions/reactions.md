If thwe bot sees certain keywords, react a certain way
- sees words `joe` - respond with `who ims joe`
- sees `joe biden` - respond `:dark-brandon:`
- sees `joe mama` - respond `:angry-crying:`
- sees `kazuma` - respond `Hai, Kazuma Desu :aqua-crying`
- sees `cheems` - respond with `cheemsburbger :cheesburger:`
- sees `bonk` - respond with `put these horny ambitions to rest :bonk:`