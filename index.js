// Load environment variables from .env file
require('dotenv').config()

// Keybase Imports
const Bot = require('keybase-bot')
const keybaseBot = new Bot()

// Twitter Imports
const { TwitterApi } = require('twitter-api-v2');
const clientTwitter = new TwitterApi(process.env.BEARER_TOKEN);
const clientTwitterRO = clientTwitter.readOnly;
const seenTweetIds = new Set(); // Set to store seen tweet IDs

// Function to monitor tweets from a specific user
async function monitorUserTweets(username) {
  console.log(`Monitoring tweets from @${username}`);

  // Set up parameters for user timeline
  const params = {
    'tweet.fields': ['created_at'],
    'user.fields': ['username'],
    max_results: 10,  // Adjust as needed
  };

  // Function to check for new tweets
  async function checkForNewTweets() {
    try {
      const userTimeline = await clientTwitterRO.v2.userTimeline(username, params);
      const tweets = userTimeline.data.data;

      if (tweets && tweets.length > 0) {
        // Reverse the array to process older tweets first
        tweets.reverse().forEach(async tweet => {
          if (!seenTweetIds.has(tweet.id)) {
            seenTweetIds.add(tweet.id);
            const tweetUrl = `https://twitter.com/${username}/status/${tweet.id}`;
            console.log('New tweet URL:', tweetUrl);

            // Send to Keybase
            const channel = { name: 'pp_esports', membersType: 'team', topicName: 'ye' }
            const message = { body: tweetUrl }
            await keybaseBot.chat.send(channel, message)
          }
        });
      }
    } catch (error) {
      console.error('Error checking for new tweets:', error);
    }
  }

  // Initial check
  await checkForNewTweets();

  // Check for new tweets every 8 hours (adjust as needed)
  setInterval(async () => {
    await checkForNewTweets(username, params);
  }, 28800000); // 8 hours in milliseconds
}

async function main() {
  const username = process.env.KB_USERNAME
  const paperkey = process.env.KB_PAPERKEY
  
  try {
    await keybaseBot.init(username, paperkey, {verbose: false})
    console.log(`Your bot is initialized. It is logged in as ${keybaseBot.myInfo().username}`)

    // Monitor tweets from a specific user
    await monitorUserTweets("kanyewest")

  } catch (error) {
    console.error(error)
  } 
}

function shutDown() {
  keybaseBot.deinit().then(() => process.exit())
}

process.on('SIGINT', shutDown)
process.on('SIGTERM', shutDown)

main()