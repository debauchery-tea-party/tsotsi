FROM keybaseio/client:nightly-node
RUN mkdir /app && chown keybase:keybase /app
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
CMD node /app/index.js