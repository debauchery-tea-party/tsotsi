# tsotsi

`tsotsi` is a JavaScript Keybase bot, designed to convert Reddit links to the official `teddit `instance. `teddit` is a privacy-focused, open source alternative to Reddit. You can find more information about teddit on their [GitHub repository](https://github.com/teddit-net/teddit).

This bot is created using the [keybase-bot](https://github.com/keybase/keybase-bot) library.

## Donations
If this bot has helped you out, consider donating! :) 
### Fiat (Paypal/Venmo/Card)
 <a href='https://ko-fi.com/Q5Q81LOP9' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://storage.ko-fi.com/cdn/kofi4.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
### Bitcoin/Monero
 <a href='https://btcpay.atemosta.com/apps/TiJEH5GCqtfVK8QkqcYksW8j3G1/pos' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://btcpay.atemosta.com/img/paybutton/pay.svg' border='0' alt='Buy Me a Coffee at btcpay.atemosta.com' /></a>


## Installation
To install and run tsotsi, follow these steps:

1. Copy the `.env.example` file to `.env`
```
cp .env.example .env
```

2. Replace with your own values
```
sudo nano .env
```

3. Run the following command to start the docker container:

```
docker-compose up -d
```
## Invite tsotsi

You can also invite `tsotsi`  to your chat or team on Keybase by visting its [keybase page]((https://keybase.io/tsotsi)).

## Support
Feel free to create any issues or pull requests for expanding `tsotsi`'s functionality.

Enjoy using `tsotsi`!

## Feature Roadmap
- follow twitter accounts
- help function to break down commands
- respond to text in a specified way
- set alarms
- do math equations
- get weather for zip code
- when friend goes live on twitch, post link
- add birthdays
- birthday reminders
- ask questions via openai api key
- Giphy???
    - Add gifs based on keywords

